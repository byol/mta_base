"use strict";
var express = require("express");
var hdbext = require('@sap/hdbext');
var xsenv = require('@sap/xsenv');

var hanaConfig = xsenv.cfServiceCredentials({ tag: 'hana' });

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

module.exports = function(){
	var app = express(); 
	
	app.use('/gethana', hdbext.middleware(hanaConfig));

	app.route("/")	// /nodejs/
	  .get(function(req, res) {
	    res.send("Hello Get ODATA World Node.js <a href=\"gethana\">Get HANA Data</a>");
	});
	
	// Note the for this service URL detail discovery to work you must define a user provided service
	// xs cups sapcp-http-odata -p '{"url":"https://sfp00maind0cd77f9b.us2.hana.ondemand.com/http/odata/http.xsodata/","type":"SAPCP"}'

	app.route("/getodata")	// /nodejs/getodata
	  .get(function(req, res) {
	  	var url = "";
	  	/*
		var svcs = JSON.parse(process.env.VCAP_SERVICES);
		var creds = svcs["user-provided"][0].credentials;
		
		url = creds.url;

		console.log("HTTP ODATA URL = " + url + "");

		request({
			url: url + "connection/",
			method: "GET",
			headers: {
				"Accept": "application/json"
			}
		}, function(error, response, body) {
			if (response.statusCode == 200) {
				//console.log("Body:\n" + body + "\n");
				var jb = JSON.parse(body);
				if (jb.d.results.length > 0) {
					for(var i=0; i < jb.d.results.length; i++) {
						var conn = jb.d.results[i];
						var method = conn.method;
						var body = conn.resbody;
						console.log("Conn[" + i + "] " + method + " " + body + "\n");
					}
				} else {
					console.log("No Connection Data");
				}
			} else {
				console.log(error);
			}

		});
		*/
	    res.send("Get ODATA URL : " + url);
	});
	
	app.route("/gethana")	// /nodejs/gethana
	  .get(function(req, res, next) {
	  	
	  	var results = "";
	  	var sql = "";
	  	sql = 'SELECT "tempId","tempVal","ts","created" FROM "mta_base.db::data.temp"';
	  	req.db.exec(sql, function (err, rows) {
			if (err) { return next(err); }
			for(var i=0; i < rows.length; i++) {
				results += "row[" + i + "]: " + rows[i].tempId + " " + rows[i].tempVal + " " + rows[i].ts + " " + rows[i].created + "<br />\n";
			}
			res.send('Temps: <br />\n' + results);
	  	});
	});
	

	
   return app;	
};