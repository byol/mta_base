$.response.contentType = "text/html";
var body = "";

body += "<html>\n";
body += "<head>\n";
body += "</head>\n";
body += "<body style=\"font-family: Tahoma, Geneva, sans-serif\">\n";

var gconn = {}; //Global DB gconnection
var output = "";

function randomIntFromInterval(min,max)
{
    return Math.floor(Math.random()*(max-min+1)+min);
}

function insertTEMP() {
	var sql = "";

	var tempVal = randomIntFromInterval(70,110);
	var ts = (new Date()).getUTCDate();
	
	/*
	INSERT INTO "mta_base.db::data.temp" VALUES(
	2,
	101,
	'2017-06-15 12:00:00',
	CURRENT_UTCTIMESTAMP);
	*/
	
	sql = "insert into \"mta_base.db::data.temp\" values(\"mta_base.db::tempId\".NEXTVAL," + tempVal + ",'" + ts + "',CURRENT_UTCTIMESTAMP)";

	var pstmt = gconn.prepareStatement(sql);
	var result = pstmt.execute();

	gconn.commit();	//Don't forget to commit!

	pstmt.close();
	
	return(tempVal);
}

function getTemps() {

	var sql = "";
	var results = "";
	
	/*
SELECT TOP 1000
	"tempId",
	"tempVal",
	"ts",
	"created"
FROM "MTA_BASE_HDI_CONTAINER_1"."mta_base.db::data.temp";	
	*/
	sql = 'SELECT "tempId","tempVal","ts","created" FROM "mta_base.db::data.temp"';
	
	var pstmt = gconn.prepareStatement(sql);
	var rs = pstmt.executeQuery();
	
	while (rs.next()) {
		results += "id:  " + rs.getInteger(1) + "<br />\n";
		results += "val: " + rs.getInteger(2) + "<br />\n";
		results += "ts:  " + rs.getString(3) + "<br />\n";
		results += "cr:  " + rs.getString(4) + "<br />\n";
		results += "<br />\n";
	}
	
	rs.close();
	pstmt.close();
	
	return(results);
}

try {
	// Always assume and exception can occur

	var i=0;
	var strJSON = "";
	
	//var request_method = $.request.getMethod();	// Deprecated syntax doesn't work!?
	var request_method = $.request.method;
	var method = "";
	
	method += $.request.toString();

	switch(request_method) {
	case $.net.http.GET:	// GET == 1
		method = "GET";
		//var jsonParam = $.request.getParameter("json"); // Deprecated syntax doesn't work!
		strJSON = "";
		// Parameters from the HTTP GET query string
		for (i=0; i<$.request.parameters.length; ++i) {
		    var name = $.request.parameters[i].name;
		    var value = $.request.parameters[i].value;
		    if (name === "json") {
				strJSON = value;
				// method += "json: " + value;
		    }
		    else {
				method += " only param named json accepted. not " + name;
		    }
		}
		if (strJSON !== "") {
			method += " " + strJSON;
		}
		else {
			method += " empty param named json.";
		}
		break;
	case 2: // Unknown
		method = "Unknown";
		break;
	case $.net.http.POST: // POST = 3
		method = "POST";
		
		// Loop through all the HTTP headers and only process the body if content-type is application/json
		var content_type = "";
		for (i=0; i<$.request.headers.length; ++i) {
		    var name = $.request.headers[i].name;
		    var value = $.request.headers[i].value;

		    //method += " header[" + i + "] = " + name + ":" + value + "\n";

		    if (name === "content-type") {
				content_type = value;
			    method += " header[" + i + "] = " + name + ":" + value + "\n";
		    }
 
		}
		if (content_type.indexOf("application/json") !== -1) {
			// Extract the body of the POST
			strJSON = $.request.body.asString();
			//method += " POSTED BODY: " + strJSON;
		}
		else {
			method += "\n ERROR: expected content-type:applicaton/json.";
		}
		
		break;
	case $.net.http.PUT: // PUT = 4
		method = "PUT";
		break;
	case $.net.http.DEL: // DELETE = 5
		method = "DELETE";
		break;
	case 0: // OPTIONS
		method = "OPTIONS";
		break;
	case $.net.http.gconnECT: // gconnECT = 7
		method = "gconnECT";
		break;
	default:
		method = "Unhandled";
		break;
	}

	gconn = $.db.getConnection();
	
	output += "\n";	

	output += "Insert Temp<br />\n";
		
	output += "Temp Inserted was " + insertTEMP() + "<br /><br />\n";

	output += "Read Temps<br />\n";

	output += getTemps();	

	output += "\nFINISHED... \n";
	

} catch (exception) {
	// Figure out what kind of exception it is and handle ones we care about.
	if (exception instanceof TypeError) {
		// Handle TypeError exception
		output += "TypeError: " + exception.toString() + "\n";
	} else if (exception instanceof ReferenceError) {
		// Handle ReferenceError
		output += "ReferenceError: " + exception.toString() + "\n";
	} else if (exception instanceof SyntaxError) {
		// Handle SyntaxError
		output += "SyntaxError: " + exception.toString() + "\n";
	} else {
		// Handle all other not handled exceptions
		output += "ExceptionError: " + exception.toString() + "\n";
	}
	
} finally {
	// This code is always executed even if an exception is 
	if (gconn instanceof Object) {
		gconn.close();
	}
}

body += output;

body += "</body>\n";
body += "</html>\n";

$.response.setBody(body);
